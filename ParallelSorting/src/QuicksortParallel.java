import java.util.Arrays;
import java.util.concurrent.RecursiveAction;

public class QuicksortParallel extends RecursiveAction {
	private int[] arr;
    private int start, end;
    private int threshold;
    private int pivotPos;

    public QuicksortParallel(int[] arr, int start, int end, int threshold) {
            this.arr = arr;
            this.start = start;
            this.end = end;
            this.threshold = threshold;
    }

    @Override
    protected void compute() {
    	if(end-start <= 0) {
    		return;	
    	}
    	if (end - start <= threshold) {
    		// sequential sort
    		Arrays.sort(arr, start, end + 1);
    		return;
    	}
    	
    	// run quicksort on the current array
    	quicksort();

    	// Sort halves in parallel
    	invokeAll(
    		new QuicksortParallel(arr, start, pivotPos-1, threshold),
    		new QuicksortParallel(arr, pivotPos+1, end, threshold));
    }

    private void quicksort() {
    	int pivot = arr[start];		//Use the first element as the pivot
    	int leftPos = start+1;		//Left cursor starts next to the pivot
    	int rightPos = end;			//Right cursor starts at end
    	boolean finished = false;	//Boolean to keep track of when the quicksort ends
    		
    	while(!finished) {
    		//Loop to find an int that is smaller than the pivot
    		while(leftPos <= rightPos && arr[leftPos] <= pivot) {
        		leftPos++;
    		}
    		//Loop to find an in that is larger than the pivot
    		while(leftPos <= rightPos && pivot <= arr[rightPos]) {
        			rightPos--;
    		}
    		
    		if(rightPos < leftPos) {	//Check if the right cursor has passed the left cursor
    			//If so, the current iteration of quicksort has ended
    			finished = true;
    		} else {
    			//Swap the smaller and larger integers
    			swapElements(leftPos, rightPos);
    			leftPos++;
    			rightPos--;
    		}
    	}
    	//Swap the pivot and the right-most integer that is smaller than the pivot
    	swapElements(start, rightPos);
    	pivotPos = rightPos;
    }
    
    /* swapElements(int i. int j) - swaps two elements in arr 
	 * 	Arguments:
	 * 		int i - the index of the first item to be swapped
	 * 		int j - the index of the second item to be swapped*/
    private void swapElements(int i, int j) {
    	int temp = arr[i];
    	arr[i] = arr[j];
    	arr[j] = temp;
    }
}
