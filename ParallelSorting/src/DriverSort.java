import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class DriverSort {
	static long startTime = 0;

	/*	tick() - stores the current system time in the variable startTime */
	private static void tick(){
		startTime = System.currentTimeMillis();
	}

	/*	toc() - returns a float representing the time between the startTime and the current system time. */
	private static float toc(){
		return (System.currentTimeMillis() - startTime) / 1000.0f; 
	}

	/* randomArrat(int size) - creates and returns a random int array of length size
	 * 	Arguments:
	 * 		int size - integer denoting the size of the array to be created*/
	private static int[] randomArray (int size) {
		int[] array = new int[size];
		Random randomGen = new Random();
		for(int index = 0; index < size; index++) {
			int randomInt = randomGen.nextInt(size);
			array[index] = randomInt;
		}
		return array;
	}

	/* arrayShuffle(int[] array) - returns a shuffled version of the argument array 
	 * 	Arguments:
	 * 		int[] array - the integer array to be shuffled */
	private static void arrayShuffle (int[] array) {
		Random randomGen = new Random();
		for(int i = 0; i < array.length; i++) {
			int randomPos = randomGen.nextInt(array.length);
			int temp = array[i];
			array[i] = array[randomPos];
			array[randomPos] = temp;
		}
	}

	/* main(String[] args) - main method that benchmarks MergesortParallel and QuicksortParallel
	 * 	Arguments:
	 *		String sort: word specifying which sorting algorithm to use: Mergesort or Quicksort
	 *		int arraySizeMin: size of the smallest (random) array of integers to be sorted
	 *		int arraySizeMax: size of the largest (random) array of integers to be sorted
	 *		int arraySizeIncr: the step size for increasing the array of integers to be sorted
	 *		String outFileName: the name of the file to which the output data will be written*/
	public static void main(String [] args) {
		int arraySizeMin = -1, arraySizeMax = -1, arraySizeIncr = -1;
		String sort, outFileName;
		//Check if the user has input the correct number of arguments
		System.out.print("Reading in inputs");
		if(args.length != 5) {
			System.err.println("Wrong number of arguments.");
			return;
		}
		//Check if the user specified a correct sort type
		if(args[0].equals("Mergesort") || args[0].equals("Quicksort")) {
			sort = args[0];
		} else {
			System.err.println("Argument " + args[0] + " must be either 'Mergesort' or 'Quicksort'");
			return;
		}
		//Check if the user input integers for arraySizeMin, arraySizeMax, and arraySizeIncr
		try {
			arraySizeMin = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			System.err.println("Argument " + args[1] + " must be an integer.");
			return;
		}
		try {
			arraySizeMax = Integer.parseInt(args[2]);
		} catch (NumberFormatException e) {
			System.err.println("Argument " + args[2] + " must be an integer.");
			return;
		}
		try {
			arraySizeIncr = Integer.parseInt(args[3]);
		} catch (NumberFormatException e) {
			System.err.println("Argument " + args[3] + " must be an integer.");
			return;
		}
		outFileName = args[4];
		System.out.println("...done.");
		
		//Output file creation and setup
		System.out.print("Creating output file");
		File outFile = new File(outFileName);
		PrintWriter writer;
		try {
			writer = new PrintWriter(outFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		writer.println(sort);
		writer.println("arraySize	optimalNumThreads	bestTime	bestSpeedup");
		System.out.println("...done.");

		System.out.print("Creating array of random ints");
		int [] arr = randomArray(arraySizeMax); //Create random array of int
		System.out.println("...done.");

		float bestTimePara = -1;	//Variable to keep track of the best time for each parallel sort of the respective array 
		float bestTimeSeq = -1;		//Variable to keep track of the best time for each sequential sort of the respective array 
		int optNumThreads = -1; 	//Variable to keep track of the optimal number of threads for each array size

		System.out.print("Sorting for different sizes, number of threads, and iterations");
		for(int arraySize = arraySizeMin; arraySize <= arraySizeMax; arraySize += arraySizeIncr) {	//For loop for differing array sizes
			for(int nThreads = 2; nThreads <= 128; nThreads *=2) {	//For loop for differing number of threads
				int cutoff = arraySize/nThreads;	//Variable to keep track of the sequential cutoff, necessary to determine # of threads
				for(int i = 0; i < 5; i++) {	//For loop to run 5 iterations of each combination of arraySize + number of threads being used
					int[] arrCopy = Arrays.copyOf(arr, arr.length);
					//Timing the sequential sorting of the array 
					System.gc();
					tick();
					Arrays.sort(arrCopy);
					float seqTime = toc();
					if(seqTime < bestTimeSeq || bestTimeSeq == -1){
						bestTimeSeq = seqTime;
					}
					//Parallel sorting of the array
					if(sort.equals("Mergesort")) {
						MergesortParallel mergesort = new MergesortParallel(arr, 0, arr.length, cutoff);
						System.gc();
						tick();
						ForkJoinPool.commonPool().invoke(mergesort);
						float time = toc();
						if(time < bestTimePara || bestTimePara == -1) {		//Update best parallel time
							bestTimePara = time;
							optNumThreads = nThreads;
						}
					} else if (sort.equals("Quicksort")) {
						QuicksortParallel quicksort = new QuicksortParallel(arr, 0, arr.length - 1, cutoff);
						System.gc();
						tick();
						ForkJoinPool.commonPool().invoke(quicksort);
						float time = toc();
						if(time < bestTimePara || bestTimePara == -1) {		//Update best parallel time
							bestTimePara = time;
							optNumThreads = nThreads;
						}
					}
					arrayShuffle(arr);	//shuffle the array for next iteration
				}
			}
			writer.println(arraySize + "			" + optNumThreads + "		" + bestTimePara + "		" + (bestTimeSeq/bestTimePara));
			//Reset variables for the next iteration
			bestTimePara = -1;
			bestTimeSeq = -1;
			optNumThreads = -1;
		}
		System.out.println("...done.");

		writer.close();
		System.out.println("All done.");
	}
}
