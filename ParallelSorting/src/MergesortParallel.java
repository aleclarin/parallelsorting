import java.util.Arrays;
import java.util.concurrent.RecursiveAction;


public class MergesortParallel extends RecursiveAction {
	private int[] arr;
    private int start, end;
    private int threshold;

    public MergesortParallel(int[] arr, int start, int end, int threshold) {
            this.arr = arr;
            this.start = start;
            this.end = end;
            this.threshold = threshold;
    }

    @Override
    protected void compute() {
    	if (end - start <= threshold) {
    		// sequential sort
    		Arrays.sort(arr, start, end);
    		return;
    	}

    	// Sort halves in parallel
    	int mid = start + (end-start) / 2;
    	invokeAll(
    			new MergesortParallel(arr, start, mid, threshold),
    			new MergesortParallel(arr, mid, end, threshold) );

    	// sequential merge
    	sequentialMerge(mid);
    }

    private void sequentialMerge(int mid) {
         //implement the merge
    	if(end-start <= 1) {
    		return;	//Return if the array is already sorted
    	}
    	
    	int[] temp = new int[end-start];	
    	int index = 0, i = start, j = mid;
    	
    	while(i < mid && j < end) {
			if(arr[i] < arr[j]) {
				//The number in the first half of the array is smaller
				temp[index] = arr[i];	
				index++;
				i++;
			} else {
				//The number in the second half of the array is smaller
				temp[index] = arr[j];
				index++;
				j++;
			}
    	}
    	while(i < mid) {
    		//Add all remaining items in the first half of the array
    		temp[index] = arr[i];
    		index++;
    		i++;
    	}
    	while(j < end) {
    		//Add all remaining items in the second half of the array
    		temp[index] = arr[j];
    		index++;
    		j++;
    	}
    	for(int k = start, l = 0; k < end; k++, l++) {
    		//Copy the sorted array temp into arr
    		arr[k] = temp[l];
    	}
    }
}
