import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

import org.junit.Test;

public class TestMergesortParallel {
	@Test
	public void test() {
		//Create two arrays to test on
		int[] seqTest = new int[1000000];
		int[] mergesortTest = new int[1000000];
		
		//Randomly make an array 1000000 integers long
		Random randomGen = new Random();
		for(int index = 0; index < 1000000; index++) {
			int randomInt = randomGen.nextInt(1000000);
			seqTest[index] = randomInt;
		}
		
		//Copy the array
		mergesortTest = Arrays.copyOf(seqTest, seqTest.length);
		
		//Sort both arrays
		Arrays.sort(seqTest);
		MergesortParallel mergesort = new MergesortParallel(mergesortTest, 0, mergesortTest.length, 2);
		ForkJoinPool.commonPool().invoke(mergesort);
		
		//Assert that the sorted results are equal
		assertArrayEquals(seqTest, mergesortTest);
	}
}
