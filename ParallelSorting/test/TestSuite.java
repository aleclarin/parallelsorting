import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestMergesortParallel.class, TestQuicksortParallel.class })
public class TestSuite {

}
